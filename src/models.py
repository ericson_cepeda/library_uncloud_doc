class LocalData(DynamicDocument):
    meta = {'allow_inheritance': True}

    def __unicode__(self):
        return self.title


class CatalogEntity(DynamicEmbeddedDocument):
    meta = {'allow_inheritance': True}


class LocalBook(Document):
    dewey = StringField()
    category = StringField()
    author = StringField()
    title = StringField()
    catalog_entity = EmbeddedDocumentField(CatalogEntity)
    local_data = ReferenceField(LocalData)

    meta = {
        'indexes': [
            {'fields': ['dewey', 'category', 'author', 'catalog_entity']},
        ]
    }


class RemoteEntity(Document):
    name = StringField()
    properties = DictField()
    date_created = DateTimeField(default=datetime.now)
    date_modified = DateTimeField(default=datetime.now)
    meta = {'allow_inheritance': True}

class ArxivCategory(Document):
    name = StringField()
    properties = DictField()
    date_created = DateTimeField(default=datetime.now)
    date_modified = DateTimeField(default=datetime.now)
    meta = {'allow_inheritance': True}


class RemoteBook(DynamicDocument):
    source_id = StringField(required=True, unique=True)
    date_created = DateTimeField(default=datetime.now)
    date_modified = DateTimeField(default=datetime.now)
    sources_data = ListField()
    properties = DictField()
    measures = DictField(default={})
    meta = {
        'allow_inheritance': True,
        'indexes': [
            {'fields': ['source_id']},
        ]
    }


class SourceData(DynamicDocument):
    remote_entity = ReferenceField(RemoteEntity)
    category = DictField(default={})
    date_created = DateTimeField(default=datetime.now)
    date_modified = DateTimeField(default=datetime.now)

    meta = {'allow_inheritance': True}


class LocalBookRecommendation(DynamicDocument):
    local_book = ReferenceField(LocalBook)
    translated_category = StringField(default="")
    translated_title = StringField(default="")
    sources_data = DictField()
    date_created = DateTimeField(default=datetime.now)
    date_modified = DateTimeField(default=datetime.now)

    meta = {
        'indexes': [
            {'fields': ['local_book']},
        ]
    }