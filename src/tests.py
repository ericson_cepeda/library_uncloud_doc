class TestInterfaceHome(SeleniumLiveTestCase):
    
    def setup(self, url):
        DBNAME = "library"
        connect(DBNAME)
        self.driver.get("%s%s" % (self.live_server_url, url))
        self.assertEquals(self.driver.get_title(), 'Library Uncloud')
        
    def test_search_by_parameter(self):
        self.setup("")
        self.driver.type_in('input#search_term', 'fundamentals')
        self.driver.click('a#main_search_button')
        self.driver.wait_element_present('#result_items')
        expected_results = self.driver.find_elements_by_css_selector('#result_items > .span10')
        self.assertTrue(len(expected_results) is 10, "Not found")

class TestAPI(SeleniumLiveTestCase):
    
    def setup_domain(self, url):
        DBNAME = "library"
        connect(DBNAME)
        self.driver.get('http://library-uncloud.virtual.uniandes.edu.co%s' % url)
        
    def test_build_recommendations(self):
        book_id = "517f39961d41c80e9e2c9a3f"
        self.setup_domain("/source/ask_dewey?book_id=%s" % book_id)
        book = LocalBook.objects.get(id=str(book_id))
        book_recommendation = LocalBookRecommendation.objects(local_book=book)

    def test_concept_similarity(self):
        self.setup_domain("/")
        lang = LanguageAnalyzer()
        similarity = lang.get_concept_similarity('car', "engine")
        self.assertEqual(0, similarity)